<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    protected $user;
   protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /** @test  */
    public function is_this_an_instance_of_user()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(User::class, $this->user);
    }

    /** @test */
    public function can_create_user()
    {
        $this->withoutExceptionHandling();

        $user = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'phone_number' => $this->faker->phoneNumber,
            'profile_photo' => $this->faker->image(null, 150,150),
            'role_id' => 1,
            'email' => $this->faker->safeEmail,
            'email_verified_at' => now()->toDateTimeString(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => $this->faker->text(8)
        ];

        $response = $this->post(route('user.store'), $user);
        $response->assertStatus(201);
    }

    /** @test */
    public function a_user_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $response = $this->put(route('user.update', $this->user->id),[
            'first_name' => 'Fulufhelo',
            'last_name' => $this->faker->lastName,
            'phone_number' => $this->faker->phoneNumber,
            'profile_photo' => $this->faker->image(null, 150,150),
            'role_id' => 1,
            'email' => $this->faker->safeEmail,
            'email_verified_at' => now()->toDateTimeString(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => $this->faker->text(8)
        ]);

        $response->assertOk();
    }

    /** @test */
    public function a_user_can_be_viewed()
    {
        $this->withoutExceptionHandling();
        $response = $this->get(route('user.show', $this->user->id));
        $response->assertJsonStructure(['data']);
        $response->assertOk();
    }

    /** @test */
    public function list_all_the_users()
    {
        $this->withoutExceptionHandling();
        factory(User::class,9)->create();
        $response = $this->get(route('user.index'));
        $response->assertJsonCount(3);
    }

    /** @test */
    public function a_user_can_be_deleted()
    {
        $this->withoutExceptionHandling();
        $response = $this->delete(route('user.destroy',$this->user->id));
        $response->assertStatus(204);
    }
}
