<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Referral;

class ReferralTest extends TestCase
{
    use RefreshDatabase;
    protected $referral;
    protected function setUp(): void
    {
        parent::setUp();

        $this->referral = factory(Referral::class)->create();
    }

    /** @test */
    public function is_an_instance_of_referral()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(Referral::class, $this->referral);
    }
}
