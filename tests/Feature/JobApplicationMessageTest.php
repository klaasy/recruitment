<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\JobApplicationMessage;

class JobApplicationMessageTest extends TestCase
{
    use RefreshDatabase;
    protected $job_application_message;
    protected function setUp(): void
    {
        parent::setUp();

        $this->job_application_message = factory(JobApplicationMessage::class)->create();
    }

    /** @test */
    public function is_an_instance_of_job_application_message()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(JobApplicationMessage::class,$this->job_application_message);
    }
}
