<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\JobApplicationStatus;

class JobApplicationStatusTest extends TestCase
{
   use RefreshDatabase;
   use WithFaker;
   protected $job_application_test;
   protected function setUp(): void
   {
       parent::setUp(); // TODO: Change the autogenerated stub
       JobApplicationStatus::create([
           'name' => $this->faker->text(50)
       ]);

       $this->job_application_test = JobApplicationStatus::first();
   }

   /** @test */
    public function is_an_instance_of_job_application_status()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(JobApplicationStatus::class, $this->job_application_test);
   }

   /** @test */
    public function string_fields_of_job_application_status()
    {
        $this->withoutExceptionHandling();
        $this->assertIsString($this->job_application_test->name);
   }
}
