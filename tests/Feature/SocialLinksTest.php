<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\SocialLink;

class SocialLinksTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    protected $social_links;
    protected function setUp(): void
    {
        parent::setUp();

        $this->social_links = factory(SocialLink::class)->create();
    }

    /** @test */
    public function is_an_instance_of()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(SocialLink::class, $this->social_links);
    }

    /** @test */
    public function string_fields_of_social_link()
    {
        $this->withoutExceptionHandling();
        $this->assertIsString($this->social_links->name);
        $this->assertIsString($this->social_links->link);
    }

    /** @test */
    public function integer_fields_of_social_link()
    {
        $this->withoutExceptionHandling();
        $this->assertIsInt($this->social_links->candidate_id);
    }
}
