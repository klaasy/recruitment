<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\JobApplication;

class JobApplicationTest extends TestCase
{
   use RefreshDatabase;
   protected $job_application;
   protected function setUp(): void
   {
       parent::setUp();

       $this->job_application = factory(JobApplication::class)->create();
   }

   /** @test */
    public function is_an_instance_of_job_application()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(JobApplication::class, $this->job_application);
   }
}
