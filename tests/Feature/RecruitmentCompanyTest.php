<?php

namespace Tests\Feature;

use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\RecruitmentCompany;

class RecruitmentCompanyTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $recruitment_company;

    protected function setUp(): void
    {
        parent::setUp();

        $this->recruitment_company = factory(RecruitmentCompany::class)->create();
    }

    /** @test */
    public function is_an_instance_of_recruitment_company()
    {
        $this->withoutExceptionHandling();
        $this->assertInstanceOf(RecruitmentCompany::class, $this->recruitment_company);
    }
}
