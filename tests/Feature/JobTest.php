<?php

namespace Tests\Feature;

use App\ClientJob;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JobTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    protected $job;

    protected function setUp(): void
    {
        parent::setUp();

        $this->job = factory(ClientJob::class)->create();
    }

    /** @test */
    public function is_an_instance_of_job()
    {
        $this->withoutExceptionHandling();

        $this->assertInstanceOf(ClientJob::class, $this->job);
    }
}
