<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $guarded = [];
    protected $dates = ['start_date', 'end_date'];

    public function setStartDateAttribute($start_date)
    {
        return $this->attributes['start_date'] = Carbon::parse($start_date);
    }

    public function setEndDateAttribute($end_date)
    {
        return $this->attributes['end_date'] = Carbon::parse($end_date);
    }
}
