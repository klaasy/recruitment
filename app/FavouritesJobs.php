<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouritesJobs extends Model
{
    protected $guarded = [];

    public function client_job()
    {
        return $this->belongsTo(ClientJob::class, 'client_job_id');
    }

    public function candidate()
    {
        return $this->belongsTo(Candidate::class, 'candidate_id');
    }
}
