<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualificationType extends Model
{
    protected $guarded = [];
}
