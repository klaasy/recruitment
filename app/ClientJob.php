<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientJob extends Model
{
    protected $guarded = [];

    public function recruitment_client()
    {
        return $this->belongsTo(RecruitmentClient::class, 'recruitment_client_id');
    }

    public function recruitment_company()
    {
        return $this->belongsTo(RecruitmentCompany::class, 'recruitment_company_id');
    }

    public function employment_type()
    {
        return $this->belongsTo(EmployeeType::class, 'employment_type_id');
    }

    public function experience_level()
    {
        return $this->belongsTo(ExperienceLevel::class, 'experience_level_id');
    }

    public function job_function()
    {
        return $this->belongsTo(JobFunction::class, 'job_function_id');
    }

    public function company_indusry()
    {
        return $this->belongsTo(CompanyIndustry::class, 'company_industry_id');
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function referrals()
    {
        return $this->hasMany(Referral::class);
    }

    public function job_applications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function job_skills()
    {
        return $this->hasMany(JobSkill::class);
    }
}
