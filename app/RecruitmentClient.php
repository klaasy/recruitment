<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecruitmentClient extends Model
{
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function recruitment_company()
    {
        return $this->belongsTo(RecruitmentCompany::class, 'recruitment_company_id');
    }
}
