<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateSkill extends Model
{
    protected $guarded = [];

    public function skill()
    {
        return $this->belongsTo(Skill::class, 'skill_id');
    }
}
