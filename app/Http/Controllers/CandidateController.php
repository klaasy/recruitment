<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Http\Resources\CandidateResource;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function index()
    {
        //factory(Candidate::class, 10)->create();
        $candidates = Candidate::orderBy('id', 'desc')->paginate(15);
        return CandidateResource::collection($candidates);
    }

    public function store(Request $request)
    {
        $candidate = Candidate::create([
            'user_id' => $request->user_id,
            'bio' => $request->bio,
            'preference_description' => $request->preference_description,
            'title' => $request->title,
            'preferred_address_id' => $request->preferred_address_id,
            'current_salary' => $request->current_salary,
            'preference_salary' => $request->preference_salary,
            'date_of_birth' => $request->date_of_birth,
            'identity_number' => $request->identity_number,
        ]);

        return new CandidateResource($candidate);
    }

    public function show(Candidate $candidate)
    {
        return new CandidateResource($candidate);
    }

    public function update(Request $request, Candidate $candidate)
    {
        $candidate->update([
            'user_id' => $request->user_id,
            'bio' => $request->bio,
            'preference_description' => $request->preference_description,
            'title' => $request->title,
            'preferred_address_id' => $request->preferred_address_id,
            'current_salary' => $request->current_salary,
            'preference_salary' => $request->preference_salary,
            'date_of_birth' => $request->date_of_birth,
            'identity_number' => $request->identity_number,
        ]);

        return new CandidateResource($candidate);
    }

    public function destroy(Candidate $candidate)
    {
        $candidate->delete();

        return response()->json(null, 204);
    }
}
