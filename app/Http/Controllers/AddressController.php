<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Resources\AddressResource;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index()
    {
        //factory(Address::class, 10)->create();
        $addresses = Address::orderBy('id', 'desc')->paginate(15);

        return AddressResource::collection($addresses);
    }

    public function store(Request $request)
    {
        $address = Address::create([
            'user_id' => $request->user_id,
            'address_line_1' => $request->address_line_1,
            'address_line_2' => $request->address_line_2,
            'complex' => $request->complex,
            'city' => $request->city,
            'province' => $request->province,
            'zip_code' => $request->zip_code,
            'country' => $request->country
        ]);

        return new AddressResource($address);
    }

    public function show(Address $address)
    {
        return new AddressResource($address);
    }

    public function update(Request $request, Address $address)
    {
        $address->update([
            'user_id' => $request->user_id,
            'address_line_1' => $request->address_line_1,
            'address_line_2' => $request->address_line_2,
            'complex' => $request->complex,
            'city' => $request->city,
            'province' => $request->province,
            'zip_code' => $request->zip_code,
            'country' => $request->country
        ]);

        return new AddressResource($address);

        //return response()->json($address, 201);
    }

    public function destroy(Address $address)
    {
        $address->delete();

        return response()->json(null, 204);
    }
}
