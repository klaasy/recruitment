<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        //factory(User::class, 10)->create();

        return UserResource::collection(User::orderBy('id', 'desc')->paginate(15));
    }

    public function store(Request $request)
    {
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone_number,
            'profile_photo' => $request->profile_photo,
            'role_id' => $request->role_id,
            'email' => $request->email,
            'email_verified_at' => $request->email_verified,
            'password' => $request->password,
            'remember_token' => $request->remember_token,
        ]);

        return new UserResource($user);
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function update(Request $request, User $user)
    {
        $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone_number,
            'profile_photo' => $request->profile_photo,
            'role_id' => $request->role_id,
            'email' => $request->email,
            'email_verified_at' => $request->email_verified,
            'password' => $request->password,
            'remember_token' => $request->remember_token,
        ]);

        return new UserResource($user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }
}
