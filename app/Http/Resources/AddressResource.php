<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            //'user' => $this->user,
            'user_id' => $this->user_id,
            'address_line_1' => $this->address_line_1,
            'address_line_2' => $this->address_line_2,
            'complex' => $this->complex,
            'city' => $this->city,
            'province' => $this->province,
            'zip_code' => $this->zip_code,
            'country' => $this->country,
            //'created_date' => $this->created_at
        ];
    }
}
