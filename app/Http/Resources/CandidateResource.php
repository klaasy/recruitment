<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CandidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => $this->user,
            'bio' => $this->bio,
            'preference_description' => $this->preference_description,
            'title' => $this->title,
            'preferred_address_id' => $this->preferred_address_id,
            'preferred_address' => $this->preferred_address,
            'current_salary' => $this->current_salary,
            'preference_salary' => $this->preference_salary,
            'date_of_birth' => $this->date_of_birth->format("Y-m-d"),
            'identity_number' => $this->identity_number,
            //'created_date' => $this->created_at->diffForHumans()
        ];
    }
}
