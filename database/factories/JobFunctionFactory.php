<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\JobFunction;
use Faker\Generator as Faker;

$factory->define(JobFunction::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle
    ];
});
