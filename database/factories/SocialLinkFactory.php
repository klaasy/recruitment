<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\Model;
use App\SocialLink;
use Faker\Generator as Faker;

$factory->define(SocialLink::class, function (Faker $faker) {
    return [
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'name' => $faker->company,
        'link' => $faker->url,
        'type' => $faker->company,
    ];
});
