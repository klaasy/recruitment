<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Recruiter;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'logo' => $faker->imageUrl(150,150),
        'web' => $faker->domainName,
        'address_line_1' => $faker->streetAddress,
        'address_line_2' => $faker->streetName,
        'city' => $faker->city,
        'zip_code' => $faker->postcode,
        'recruiter_id' => $faker->numberBetween(1, Recruiter::count())
    ];
});
