<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ClientJob;
use App\CompanyIndustry;
use App\EmployeeType;
use App\ExperienceLevel;
use App\JobFunction;
use App\RecruitmentClient;
use App\RecruitmentCompany;
use Faker\Generator as Faker;

$factory->define(ClientJob::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle,
        'location' => $faker->city,
        'recruitment_company_id' => $faker->numberBetween(1, RecruitmentCompany::count()),
        'recruitment_client_id' => $faker->numberBetween(1, RecruitmentClient::count()),
        'job_description' => $faker->text(80),
        'additional_information' => $faker->text(80),
        'company_description' => $faker->text(80),
        'confidential' => $faker->numberBetween(0,1),
        'logo' => $faker->imageUrl(150,150, 'business'),
        'employment_type_id' => $faker->numberBetween(1, EmployeeType::count()),
        'experience_level_id' => $faker->numberBetween(1, ExperienceLevel::count()),
        'job_function_id' => $faker->numberBetween(1, JobFunction::count()),
        'company_industry_id' => $faker->numberBetween(1, CompanyIndustry::count()),
    ];
});
