<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Recruiter;
use App\RecruitmentCompany;
use App\User;
use Faker\Generator as Faker;

$factory->define(Recruiter::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, User::count()),
        'recruitment_company_id' => $faker->numberBetween(1, RecruitmentCompany::count())
    ];
});
