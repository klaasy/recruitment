<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RecruitmentClient;
use App\RecruitmentCompany;
use Faker\Generator as Faker;

$factory->define(RecruitmentClient::class, function (Faker $faker) {
    return [
        'client_id' => $faker->numberBetween(1, RecruitmentClient::count()),
        'recruitment_company_id' => $faker->numberBetween(1, RecruitmentCompany::count()),
    ];
});
