<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\CompanyWorkExperience;
use App\WorkExperience;
use Faker\Generator as Faker;

$factory->define(WorkExperience::class, function (Faker $faker) {
    return [
        'description' => $faker->text,
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'start_date' => $faker->dateTime('-2 years'),
        'end_date' =>$faker->dateTime('-2 months'),
        'current' => 0,
        'company_work_experience_id' => $faker->numberBetween(1, CompanyWorkExperience::count())
    ];
});
