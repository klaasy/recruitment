<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanyIndustry;
use Faker\Generator as Faker;

$factory->define(CompanyIndustry::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10)
    ];
});
