<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Candidate;
use App\ClientJob;
use App\JobApplication;
use App\JobApplicationMessage;
use App\JobApplicationStatus;
use Faker\Generator as Faker;

$factory->define(JobApplication::class, function (Faker $faker) {
    return [
        'candidate_id' => $faker->numberBetween(1, Candidate::count()),
        'job_application_status_id' => $faker->numberBetween(1, JobApplicationStatus::count()),
        //'job_application_message_id' => $faker->numberBetween(1, JobApplicationMessage::count()),
        'client_job_id' => $faker->numberBetween(1, ClientJob::count()),
    ];
});
