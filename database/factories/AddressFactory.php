<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use App\User;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, User::count()),
        'address_line_1' => $faker->streetAddress,
        'address_line_2' => $faker->streetName,
        'complex' => $faker->citySuffix,
        'city' => $faker->city,
        'province' => $faker->state,
        'zip_code' => $faker->postcode,
        'country' => $faker->country,
    ];
});
