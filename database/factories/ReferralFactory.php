<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Attachment;
use App\ClientJob;
use App\Referral;
use Faker\Generator as Faker;

$factory->define(Referral::class, function (Faker $faker) {
    return [
        'client_job_id' => $faker->numberBetween(1, ClientJob::count()),
        'attachment_id' => $faker->numberBetween(1, Attachment::count())
    ];
});
