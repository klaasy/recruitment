<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\RecruitmentCompany;
use Faker\Generator as Faker;

$factory->define(RecruitmentCompany::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'web' => $faker->domainName,
        'email' => $faker->safeEmailDomain,
        'phone' => $faker->phoneNumber,
        'address_line_1' => $faker->streetAddress,
        'address_line_2' => $faker->streetName,
        'city' => $faker->city,
        'zip_code' => $faker->postcode,
    ];
});
