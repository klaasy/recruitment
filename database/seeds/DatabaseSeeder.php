<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
        ]);

        // factory(App\Role::class, 4)->create();
        // factory(App\Address::class,15)->create();
        // factory(App\User::class, 10)->create();
        // factory(App\Candidate::class, 5)->create();
    }
}
