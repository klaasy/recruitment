<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public $roles = [   
        [ 'name' => 'Admin', 'description' => 'Administrator'],
        [ 'name' => 'Client', 'description' => 'Client'],
        [ 'name' => 'Recruiter', 'description' => 'Recruiter'],
        [ 'name' => 'Candidate', 'description' => 'Candidate']
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->roles as $role) {
            DB::table('roles')->insert([
                'name' => $role['name'],
                'description' => $role['description'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
