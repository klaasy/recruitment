<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('candidate_id');
            $table->unsignedInteger('job_application_status_id');
            //$table->unsignedInteger('job_application_message_id');
            $table->unsignedInteger('client_job_id');
            $table->timestamps();
            $table->index('candidate_id');
            $table->index('job_application_status_id');
            //$table->index('job_application_message_id');
            $table->index('client_job_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_applications');
    }
}
