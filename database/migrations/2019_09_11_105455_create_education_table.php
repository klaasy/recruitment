<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('qualification');
            $table->string('degree');
            $table->unsignedInteger('candidate_id');
            $table->boolean('completion_status');
            $table->unsignedInteger('qualification_type_id');
            $table->timestamp('start_date');
            $table->timestamp('end_date')->default(now());
            $table->string('type');
            $table->boolean('current');
            $table->string('description');
            $table->unsignedInteger('institution_id');
            $table->timestamps();
            $table->index('candidate_id');
            $table->index('qualification_type_id');
            $table->index('institution_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
