<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('location');
            $table->unsignedInteger('recruitment_client_id');
            $table->unsignedInteger('recruitment_company_id');
            $table->text('job_description');
            $table->text('additional_information');
            $table->text('company_description');
            $table->boolean('confidential');
            $table->string('logo');
            $table->unsignedInteger('employment_type_id');
            $table->unsignedInteger('experience_level_id');
            $table->unsignedInteger('job_function_id');
            $table->unsignedInteger('company_industry_id');
            $table->timestamps();
            $table->index('location');
            $table->index('recruitment_client_id');
            $table->index('recruitment_company_id');
            $table->index('employment_type_id');
            $table->index('experience_level_id');
            $table->index('job_function_id');
            $table->index('company_industry_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_jobs');
    }
}
