<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->text('bio');
            $table->text('preference_description');
            $table->string('title');
            $table->unsignedInteger('preferred_address_id');
            $table->decimal('current_salary');
            $table->decimal('preference_salary');
            $table->timestamp('date_of_birth');
            $table->string('identity_number');
            $table->timestamps();
            $table->index('user_id');
            $table->index('preferred_address_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
