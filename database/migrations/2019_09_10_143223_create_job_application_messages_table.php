<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('recruiter_id');
            $table->unsignedInteger('candidate_id');
            $table->unsignedInteger('job_application_id');
            $table->text('message');
            $table->timestamps();
            $table->index('recruiter_id');
            $table->index('candidate_id');
            $table->index('job_application_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_application_messages');
    }
}
