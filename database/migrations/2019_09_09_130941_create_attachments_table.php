<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('client_job_id');
            $table->unsignedInteger('candidate_id');
            //$table->string('file_type');
            $table->unsignedInteger('file_type_id');
            $table->string('file_name');
            $table->timestamps();
            $table->index('client_job_id');
            $table->index('candidate_id');
            $table->index('file_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
