@extends('layouts.app')
@section('css')
<style>
    
    #viewport {
        padding-left: 10px !important;
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }

    .container-fluid-cv {
        max-width: 100%;
        margin-top: 50px;
        color: #505050f2;
        text-align: left;
        border-bottom: 1.5px solid #80808036;
    }

    body {
        text-align: center;
    }

    .container-fluid-dropzone {
        max-width: 80%;
        text-align: center;
        border: 3px dashed #0087ff78;
    }

</style>
@endsection

@section('content')

    <div class="row">

        <div class="col-sm-2 col-md-2 col-lg-2">

        </div>

        {{-- CV Autofill --}}
        <div class="col-sm-6 col-md-7 col-lg-7">
            <div class="container-fluid-cv">
                <h2><strong>Use one of the options to prefill your application</strong></h2>
                <h3><small>You can still fill your profile manually</small></h3>
                <br>
                <div class="container-fluid-dropzone">
                    <i class="fas fa-cloud-upload-alt fa-3x" style="margin-top: 10px; margin-bottom: 5px"></i>
                    <h4><strong>Drag and drop your resume to autocomplete your application</strong></h4>
                    <h4><small>or</small></h4>
                    <button type="button" class="btn btn-default" style="margin-bottom: 10px">BROWSE</button>
                </div>
                <br>
            </div>
            {{-- /cv autofill --}}
            
            <div class="container-fluid-cv">
                    <h2><strong>Personal Information</strong></h2>
                    <button style="border-radius: 100%; margin-bottom:10px; margin-top:15px; float:left; margin-right:100%" type="button" class="btn btn-secondary"><i class="fas fa-user-circle fa-6x"></i></button>
                    <div class="form-group" style="float:left; margin-right:20px">
                        <label for="title">First name</label>
                        <input style="max-width:400px" type="text" class="form-control" name="first_name" id="first_name" value="">
                    </div>
                    <div class="form-group" style="display:inline-block;">
                        <label for="title">Last name</label>
                        <input style="max-width:400px" type="text" class="form-control" name="last_name" id="last_name" value="">
                    </div>
                    <br>
                    <div class="form-group" style="float:left; margin-right:20px">
                        <label for="title">Email</label>
                        <input style="max-width:400px" type="text" class="form-control" name="email" id="email" value="">
                    </div>
                    <div class="form-group" style="display:inline-block">
                        <label for="title">Confirm your email</label>
                        <input style="max-width:400px" type="text" class="form-control" name="confirm_email" id="confirm_email" value="">
                    </div>
                    <div class="form-group">
                        <label for="title">Location</label>
                        <input style="max-width:800px" type="text" class="form-control" name="location" id="location" value="">
                    </div>
                    <div class="form-group">
                        <label for="title">Phone number</label>
                        <input style="max-width:400px" type="text" class="form-control" name="phone_number" id="phone_number" value="">
                    </div>
                </div>

                <div class="container-fluid-cv">
                        <h2 style="float:left; margin-right: 400px"><strong>Experience</strong></h2>
                        <button style="display: inline-block; margin-right: 100px; margin-left: 550px;" type="button" class="btn btn-secondary"><strong>+</strong> ADD EXPERIENCE</button>
                        <i style="margin-bottom:10px; margin-top:15px; float:left; margin-right:50px" class="fas fa-building fa-6x"></i>
                        <div class="form-group" style="float:left; margin-right:20px; margin-top:50px;">
                            <label for="title">Title</label>
                            <input style="max-width:400px" type="text" class="form-control" name="title" id="title" value="">
                        </div>
                        <div class="form-group" style="display:inline-block; margin-top:50px;">
                            <label for="title">Company</label>
                            <input style="max-width:400px" type="text" class="form-control" name="company" id="company" value="">
                        </div>
                        <div class="form-group" style="margin-left:135px">
                            <label for="title">Location</label>
                            <input style="max-width:800px" type="text" class="form-control" name="location" id="location" value="">
                        </div>
                        <div class="form-group" style="margin-left:135px">
                            <label for="title">Description</label>
                            <textarea style="max-width:800px" name="description" id="description" cols="5" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group" style="float:left; margin-right:20px; margin-left:135px">
                            <label for="title">From</label>
                            <input style="max-width:400px" type="text" class="form-control" name="start_date" id="start_date" value="">
                        </div>
                        <div class="form-group" style="display:inline-block; margin-left:135px">
                            <label for="title">To</label>
                            <input style="max-width:400px" type="text" class="form-control" name="end_date" id="end_date" value="">
                        </div>
                        <label style="margin-left:135px" class="container">I currently work here
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                </div>
        
                <div class="container-fluid-cv">
                        <h2 style="float:left; margin-right: 100%"><strong>Education</strong></h2>
                        <button style="display: inline-block; margin-right: 100px; margin-left: 550px;" type="button" class="btn btn-secondary"><strong>+</strong> ADD EDUCATION</button>
                        <i style="margin-bottom:10px; margin-top:15px; float:left; margin-right:50px" class="fas fa-building fa-6x"></i>
                        <div class="form-group" style="margin-left:135px">
                            <label for="title">Institution</label>
                            <input style="max-width:800px" type="text" class="form-control" name="institution" id="institution" value="">
                        </div>
                        <div class="form-group" style="float:left; margin-right:20px; margin-top:50px; margin-left:135px">
                            <label for="title">Major</label>
                            <input style="max-width:400px" type="text" class="form-control" name="title" id="title" value="">
                        </div>
                        <div class="form-group" style="display:inline-block; margin-top:50px;">
                            <label for="title">Degree</label>
                            <input style="max-width:400px" type="text" class="form-control" name="company" id="company" value="">
                        </div>
                        <div class="form-group" style="margin-left:135px">
                            <label for="title">Location</label>
                            <input style="max-width:800px" type="text" class="form-control" name="location" id="location" value="">
                        </div>
                        <div class="form-group" style="margin-left:135px">
                            <label for="title">Description</label>
                            <textarea style="max-width:800px" name="description" id="description" cols="5" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group" style="float:left; margin-right:20px; margin-left:135px">
                            <label for="title">From</label>
                            <input style="max-width:400px" type="text" class="form-control" name="start_date" id="start_date" value="">
                        </div>
                        <div class="form-group" style="display:inline-block; margin-left:135px">
                            <label for="title">To</label>
                            <input style="max-width:400px" type="text" class="form-control" name="end_date" id="end_date" value="">
                        </div>
                        <label style="margin-left:135px" class="container">I currently attend
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
        </div>

        <div class="col-sm-4 col-md-3 col-lg-3">

        </div>
        
    </div>

@endsection

@section('scripts')
    


@endsection